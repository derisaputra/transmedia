-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 07 Agu 2016 pada 19.39
-- Versi Server: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `transmedia`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `folder`
--

CREATE TABLE IF NOT EXISTS `folder` (
`id` int(90) NOT NULL,
  `name` varchar(90) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `creator` varchar(90) NOT NULL,
  `location` varchar(90) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `folder`
--

INSERT INTO `folder` (`id`, `name`, `time`, `creator`, `location`) VALUES
(1, 'a', '2016-08-05 17:15:55', '', ''),
(2, 'awan', '2016-08-05 17:17:47', '', ''),
(3, 'asd', '2016-08-05 17:19:17', '', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `upload`
--

CREATE TABLE IF NOT EXISTS `upload` (
`id` int(20) NOT NULL,
  `filename` varchar(90) NOT NULL,
  `type` varchar(90) NOT NULL,
  `size` float NOT NULL,
  `uploader` varchar(90) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data untuk tabel `upload`
--

INSERT INTO `upload` (`id`, `filename`, `type`, `size`, `uploader`, `time`) VALUES
(4, '73229-13339567_10206587038376801_6199976936392215675_n.jpg', '62534', 61.0684, '0', '2016-08-03 16:13:38'),
(5, '23037-01.png', '196929', 192.313, '0', '2016-08-05 06:26:38');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id` int(20) NOT NULL,
  `nip` int(20) NOT NULL,
  `password` varchar(90) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `nip`, `password`) VALUES
(1, 111, '1234q'),
(5, 12345, '$2y$12$NIQ37rdx7KpZ9r/.6pO58.ycgEMOuXFIWgmYCNKxXf8lLJlVVDcY2'),
(6, 54321, '$2y$12$/9EhTBDXqe4SZMZSPq5kOeevOgPgss6dYNNZQceJCSmYuZLSTWdRO'),
(7, 0, '$2y$12$6Ae56w6nRz3gekkxlaPXgexHeaJAaCgrI4nNBkAUWFtIK5Ndm82qq'),
(8, 185, '$2y$12$spYYEawuryhOg7z57ktc.uEj.2qY4XMccqNg9yarDahCn41OC//PG'),
(9, 123456, '$2y$12$XZGXEiO7vMD1BYdhO0Nx1eJqbiIUtmnhVu0WCL1QemF9aPOgEEoSG'),
(10, 4321, '$2y$12$lL56md5E7w38FLBrttiPY.wAKHfpxg/gMTwJ8FT.rCoa/wu3I86e2'),
(11, 2147483647, '$2y$12$FufD4BaFfb22xfXgU./qG.kmywtF65hsdMgX1LlVBgu6iTU1mz3Fy'),
(12, 2147483647, '$2y$12$LXgT60IJhNnHtByA1ER.L.mn7VFL.t/CDFVpgK.1hSksQU7uVsKPy'),
(13, 2147483647, '$2y$12$na7GDhkcUDDh4UR3HzmKFeA8GgNluCBNl.X6Xd8J8BwZfljM2F51e'),
(14, 10131003, '$2y$12$.RPtHmubvmMkMp4wTDRaJ.LJTkqQE2Dry5vCXPmSVLyRYraP1h1Ia'),
(15, 103, '$2y$12$kc.kEO6euFhFf8N0YDtShepV7mjzN3Et7ncIf1cBOKzNE3m28S3Bi'),
(16, 1234, '$2y$12$JJ8wb8L09LKIHZaEieEehuc8cqKG7gF0KMGxQzdBwe48JoNQQczyq');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `folder`
--
ALTER TABLE `folder`
 ADD PRIMARY KEY (`id`,`name`);

--
-- Indexes for table `upload`
--
ALTER TABLE `upload`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `folder`
--
ALTER TABLE `folder`
MODIFY `id` int(90) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `upload`
--
ALTER TABLE `upload`
MODIFY `id` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `id` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
