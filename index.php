<?php

session_start();
if(!$_SESSION['logged']){
  header("Location: formlogin.php");
  exit;
}

// Setelah berhasil login
require_once 'php/config.php';
require 'include/header.php';
require 'include/sidebar.php';
require 'php/show_folder.php';

?>

  <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-head-line">TRANSMEDIA</h1>
                        <h1 class="page-subhead-line">MILIK KITA BERSAMA </h1>

                    </div>
                </div>
                <div class="row">
                <?php
                  foreach ($result as $r) {
                    echo '
                    <!-- /. ROW  -->
                    
                        <div id="folderlist" class="col-md-4">
                            <div class="main-box mb-red">
                                <a href="view.php?foldername='.$r["name"].'">
                                    <i class="fa fa-folder fa-5x"></i>
                                    <h5>'.$r["name"].'</h5>
                                </a>
                            </div>
                        </div>
                   
                   
                    ';
                  }
                ?>
               </div>