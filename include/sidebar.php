

 <script type="text/javascript">
    

    function add_Folder() {
    var main = document.getElementById('folderlist');
    var grid = document.createElement("div");
      grid.className = "col-md-4";
      var folder = document.createElement("div");
      folder.className = "folder";
      var img = document.createElement("div");
      img.className = "main-box mb-red";
    
      img.innerHTML = "New Folder";
      main.appendChild(grid);
      grid.appendChild(folder);
      folder.appendChild(img);
      folder.appendChild(caption);
    };


  </script>

<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">TRANSMEDIA</a>
            </div>

            <div class="header-right">

             <a href="php/logout.php" class="btn btn-danger" title="Logout"><i class="fa fa-exclamation-circle fa-2x"></i></a>

            </div>
        </nav>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <li>
                        <div class="user-img-div">
                            <img src="assets/img/user.png" class="img-thumbnail" />

                            <div class="inner-text">
                                Deri Saputra
                            <br />
                                <small>Time Log in</small>
                            </div>
                        </div>

                    </li>


                    <li>
                        <a href="javascript:void(0)" class="active-menu" id="target"><i class="fa fa-dashboard "></i>Buat Folder</a>
                    </li>
                    <li>
                      
                     <li>
                        <a href="#"><i class="fa fa-yelp "></i>Buat File<span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level">
                           
                    

            </div>

        </nav>

        <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
     <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">
          <span aria-hidden="true">&times;</span>
          <span class="sr-only">Close</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">
                    Create New Folder
                </h4>
      </div>
      <!-- Modal Body -->
      <div class="modal-body">
        <form action= "php/create_folder.php" class="form-horizontal" role="form" method="post">
          <div class="form-group">
            <div class="col-sm-12">
              <input type="text" name= "nama_folder" class="form-control" id="nama_folder" placeholder="Folder name" />
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-12">
              <button type="submit" class="btn btn-primary">Create</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script>
      $('.active-menu').click(function(event) {
  // Show the modal
  $('#myModal').modal('show');

  // Prevent default link action
  event.preventDefault();
});

      </script>