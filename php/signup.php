﻿<?php
session_start();

require_once 'config.php';

/**
 * Include ircmaxell's password_compat library.
 */
require '../lib/password.php';

if(isset($_POST['submit'])){
  //Retrieve the field values from our registration form.
  $nip = !empty($_POST['nip']) ? trim($_POST['nip']) : null;
  $pass = !empty($_POST['pass']) ? trim($_POST['pass']) : null;

  //Now, we need to check if the supplied nip already exists.
  $stmt = $pdo->prepare("SELECT COUNT(nip) AS num FROM user WHERE nip = :nip");

  //Bind the provided nip to our prepared statement.
  $stmt->bindValue(':nip', $nip);

  //Execute.
  $stmt->execute();

  //Fetch the row.
  $row = $stmt->fetch(PDO::FETCH_ASSOC);

  //If the provided nip already exists - display error.
  //TO ADD - Your own method of handling this error. For example purposes,
  //I'm just going to kill the script completely, as error handling is outside
  //the scope of this tutorial.
  if($row['num'] > 0){
    echo '<script>alert("That nip already exist!");</script>';
    header("Refresh: 0; URL = ../register.php");
    die();
  }

  //Hash the password as we do NOT want to store our passwords in plain text.
  //$pas = hash('sha256', $pass);
  $pas = password_hash($pass, PASSWORD_BCRYPT, array("cost" => 12));

  //Prepare our INSERT statement.
  //Remember: We are inserting a new row into our users table.
  $stmt = $pdo->prepare("INSERT INTO user (nip, password) VALUES (:nip, :password)");

  //Bind our variables.
  $stmt->bindValue(':nip', $nip);
  $stmt->bindValue(':password', $pas);

  //Execute the statement and insert the new account.
  $result = $stmt->execute();

  //If the signup process is successful.
  if($result){
    echo '<script>alert("Thank you for register! Now please sign in.");</script>';
    header("Refresh: 0; URL = ../formlogin.php");
  }
}
?>