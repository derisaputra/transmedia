<?php
session_start(); 
include_once 'php/dbconfig.php';
require 'include/header.php';
require 'include/sidebar.php';
 $namafolder=$_GET['foldername'];
?>

 <div id="page-wrapper">
            <div id="page-inner">

<a href="indexx.php?namefolder=<?php echo $namafolder ?>" class="btn btn-large btn-info"><i class="glyphicon glyphicon"></i>Uploads</a>
<a href="javascript:void(0)" class="delete btn btn-large btn-danger"><i class="glyphicon glyphicon"></i>Delete Folder</a>

<div class="row">
    <table class='table table-bordered table-responsive'>
   
    <tr>
     <th>ID</th>
     <th>File</th>
     <th>Size</th>
     <th>Type</th>
     <th>Uploader</th>
     <th colspan="2" align="center">Actions</th>
    </tr>
  
	
    <?php 

        $query = "SELECT * FROM folder_$namafolder";       
        $records_per_page=10;
        $newquery = $crud->paging($query,$records_per_page);
        $crud->dataview($newquery);
     ?>
      <tr>
        <td colspan="9" align="center">
            <div class="pagination-wrap">
                <?php $crud->paginglink($query,$records_per_page); ?>
            </div>
        </td>
    </tr>
    </table>
</div>
</div>    
</div>

<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                    <h4 class="modal-title custom_align" id="Heading">Delete this entry</h4>
                                </div>
                                <div class="modal-body">

                                    <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to delete this Record?</div>

                                </div>
                                <div class="modal-footer ">
                                    <button type="button" class="btn btn-success" ><a style="text-decoration:none" href="php/deletefolder.php?namefolder=<?php echo $namafolder ?>"> <span class="glyphicon glyphicon-ok-sign"></span> Yes </a> </button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <script>
      $('.delete').click(function(event) {
  // Show the modal
  $('#delete').modal('show');

  // Prevent default link action
  event.preventDefault();
});

      </script>